using System;
using MainProject;
using NUnit.Framework;

namespace CalculateTests
{
    public class CalculateTests
    {
        private Calculator _calculator;

        [SetUp]
        public void Setup()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void TestEmptyString()
        {
            //Given
            const string inputString = "";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(0, result);
        }

        [Test]
        public void TestSingleNumber()
        {
            //Given
            const string inputString = "4";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(4, result);
        }

        [Test]
        public void TestPair()
        {
            //Given
            const string inputString = "3,75";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(78, result);
        }

        [Test]
        public void TestThreeNumbers()
        {
            //Given
            const string inputString = "3,45,7";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(55, result);
        }

        [Test]
        public void TestNegativeNumber()
        {
            //Given
            var inputString = "-3,45,7";
            //When then
            Assert.Throws<Exception>(() => _calculator.Calculate(inputString));
        }


        [Test]
        public void TestGivenDefinedSeparator()
        {
            //Given
            const string inputString = "//#\n4#6#2";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(12, result);
        }

        [Test]
        public void TestGivenDefinedMultiSymbolSeparator()
        {
            //Given
            const string inputString = "//[###]\n2###3###3";
            //When
            var result = _calculator.Calculate(inputString);
            //Then
            Assert.AreEqual(8, result);
        }

        [Test]
        public void TestManySeparators()
        {
            //Given
            const string inputString = "//[##],[,],[???]\n5##88???2";
            //When
            var result = _calculator.Calculate(inputString);
            //Than
            Assert.AreEqual(95, result);
        }

        [Test]
        public void TestNSeparator()
        {
            //Given
            const string inputString = "3\n5\n2";
            //When
            var result = _calculator.Calculate(inputString);
            //Than
            Assert.AreEqual(10, result);
        }
    }
}