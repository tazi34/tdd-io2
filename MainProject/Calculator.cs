﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MainProject
{
   public class Calculator
    {
        public int Calculate(string str)
        {
            var topBoundary = 1000;
            var separators = new List<string> {",","\n"};
            string calculationString = str;

            if (str.Equals(string.Empty))
                return 0;

            if (HasDefinedSeparator(str))
            {
                string separatorString = GetSeparatorString(str);
                separators = GetSeparators(separatorString);
                calculationString = str.Substring(str.IndexOf("\n") + 1);
            }

            var splitted = calculationString.Split(separators.ToArray(), StringSplitOptions.RemoveEmptyEntries);
            int sum = 0;

            foreach (var stringNumber in splitted)
            {
                var parsedNumber = int.Parse(stringNumber);
                if (parsedNumber < 0)
                    throw new Exception();
                if (parsedNumber <= topBoundary)
                    sum += parsedNumber;
            }
            return sum;
        }

        private bool HasDefinedSeparator(string str)
        {
            if (!str.StartsWith("//"))
                return false;
            return !(str.Length < 5 || str.IndexOf("\n") < 0);
        }
        private List<string> GetSeparators(string separatorString)
        {
            if(separatorString.Length == 1)
                return new List<string>{separatorString};
            
            var separatedStrings = separatorString.Split(',');
            var separators = separatedStrings.Where(el => el.StartsWith("[") && el.EndsWith("]")).ToList();
            separators = separators.Select(el => el.Substring(1, el.Length-2)).ToList();
            if(!separators.Contains(","))
                separators.Add(",");
            if(!separators.Contains("\n"))
                separators.Add("\n");
            return separators;

        }
        private string GetSeparatorString(string str)
        {
            var pFrom = str.IndexOf("//") + 2;
            var pTo = str.LastIndexOf("\n");

            return str.Substring(pFrom, pTo - pFrom);
        }
    }
}